#!/bin/bash
# rsync deploy v3.0

source=${1%/}
dest=${2%/}

set +e # dont fail on err

 # convert ignore into rsync filter list so we don't delete them from the server.
if [ $source = "." ]; then
    cat .gitignore | sed '/^$/d; /^#/d; s/^/- /; s/^- \!/\+ /' | sed '1!G;h;$!d' > .rsync-filter
else
    cat .gitignore | sed -e '/^\/'$source'/p' -e '/^\//d' | sed 's/'$source'\///; /^$/d; /^#/d; s/^/- /; s/^- \!/\+ /' | sed '1!G;h;$!d' > .rsync-filter
fi

# exclude deployment files
echo '- /.rsync-include' >> .rsync-filter && echo '- /.rsync-filter' >> .rsync-filter && echo '- node_modules' >> .rsync-filter && echo '- /.git' >> .rsync-filter

# list non repo files created within pipeline to be included in rsync
git -C $source/ ls-files --exclude-standard -oi --directory > .rsync-include

# never include node_modules
sed -i '/node_modules/d' ./.rsync-include

# prep repo to remove files listed in .siteignore
mv -f .siteignore .gitignore

# remove siteignored files from working dir
git ls-files -i --exclude-standard --directory -z | xargs -0 rm -fr

# Logging
echo -e "ls -al:" && ls -al && echo -e "\n.rsync-filter:" && cat .rsync-filter && echo -e "\n.rsync-include:" && cat .rsync-include && echo -e "\n.gitignore (from .siteignore):" && cat .gitignore

set -e # start fail on err

# DEPLOY!
rsync -vrlz --delete --include-from=".rsync-include" --filter="merge .rsync-filter" $source/ $dest/
