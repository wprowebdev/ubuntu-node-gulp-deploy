FROM wpromote/ubuntu-node-gulp:2-fermium

ADD deploy-rsync.sh /usr/local/bin/deploy-rsync

RUN chmod +x /usr/local/bin/deploy-rsync
